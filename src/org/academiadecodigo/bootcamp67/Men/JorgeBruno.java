package org.academiadecodigo.bootcamp67.Men;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp67.AsciiStrings;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

public class JorgeBruno {
    private Prompt prompt;
    private final PrintWriter sendMessage;
    private final Socket clientSocket;
    private final String username;
    private int happyMeter = 0;

    public JorgeBruno(Prompt prompt, PrintWriter sendMessage, Socket clientSocket, String username) {
        this.prompt = prompt;
        this.sendMessage = sendMessage;
        this.clientSocket = clientSocket;
        this.username = username;
        this.happyMeter = happyMeter;
    }

    public static String bio =  "--------------------------------------------------------------------\n" +
                                "|      Sup shawty, I'm 22 years old, and I'm from Margem Sul       |\n" +
                                "|    I like to go to the gym, drink some beers, smoke some blunts  |\n" +
                                "|     I also make sick beats, you should check my Soundcloud       |\n"+
                                "--------------------------------------------------------------------\n";
    public static String sQuestion1 = "What do you do?";
    public static String sQuestion2 = "What do you listen to?";
    public static String sQuestion3 = "What are your hobbies?";
    public static String sQuestion4 = "Where would you take me on a date?";
    public static String sQuestion5 = "What would your ideal date be?";
    public static String sQuestion6 = "What are your kinks?";
    public static String sQuestion7 = "What is your drunkest memory?";
    public static String sQuestion8 = "How big are you?";
    public static String sQuestion9 = "Send me a pic!";

    public static String[] jBruno1 = {"Hey Handsome!",
                                        "Wanna smoke a blunt together?",
                                        "Do you play Roblox?",
                                        "Do you like Maria Leal?"};

    public static String[] jBruno2 = {"Manage/Work at my family’s company",
                                        "I study at Academia de Codigo",
                                        "Graphic Design is my passion",
                                        "I'm an OnlyFans content creator"};

    public static String[] jBruno3 = {"Fusion between k-pop, reggaeton and salsa",
                                        "Company Jingles",
                                        "Techno & Funk",
                                        "Indie Rock and Trap"};

    public static String[] jBruno4 = {"Skateboard", "Cooking", "Airsoft", "Streaming"};

    public static String[] jBruno5 = {"At your place", "McDonalds", "Guilty by Olivier", "A slaughterhouse"};

    public static String[] jBruno6 = {"Swing Party",
                                        "Sailing around / Boat trip",
                                        "Sushi & Drinks",
                                        "Smoking crack behind a McDonald's"};

    public static String[] jBruno7 = {"Wanna find out? ;)", "BDSM", "Ball busting", "Pegging"};

    public static String[] jBruno8 = {"I tried to steal the Monalisa",
                                        "Got drunk and installed Tinder!",
                                        "Try to get it on with my friend’s parent",
                                        "Bought a new BMW"};

    public static String[] jBruno9 = {"Bigger than you...", "Why do you wanna know?", "I’m big enough", "Come find out!"};

    public static String[] jBruno10 = {"Face", "Body", "Feet", "D*ck"};


    public int getHappyMeter() {
        return happyMeter;
    }

    public void jBrunoOne() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno1);
        scanner.setMessage(JorgeBruno.bio);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno1[answerIndex - 1]);

        switch (answerIndex) {
                                        /*Hey Handsome!",
                                        "Wanna smoke a blunt together?",
                                        "Do you play Roblox?",
                                        "Do you like Maria Leal?;*/
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 15;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        jBrunoTwo();
    }

    private void jBrunoTwo() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno2);
        scanner.setMessage(JorgeBruno.sQuestion1);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno2[answerIndex - 1]);

        switch (answerIndex) {
                /*  "Manage/Work at my family’s company",
                    "I study at Academia de Codigo",
                    "Graphic Design is my passion",
                    "I'm OnlyFans content creator"*/
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        jBrunoThree();
    }

    private void jBrunoThree() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno3);
        scanner.setMessage(JorgeBruno.sQuestion2);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno3[answerIndex - 1]);

        switch (answerIndex) {
                  /*"Fusion between k-pop, reggaeton and salsa",
                    "Company Jingles",
                    "Techno & Funk",
                    "Indie Rock and Trap"*/
            case 1:
                happyMeter += 5;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 10;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        jBrunoFour();
    }

    private void jBrunoFour() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno4);
        scanner.setMessage(JorgeBruno.sQuestion3);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno4[answerIndex - 1]);

        switch (answerIndex) {

                    /*"Skateboard",
                    "Cooking",
                    "Airsoft",
                    "Streaming"*/
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        jBrunoFive();
    }

    private void jBrunoFive() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno5);
        scanner.setMessage(JorgeBruno.sQuestion4);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno5[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nThat's me in the gym... Hope you like it! \n\n" + AsciiStrings.jBrunoGym);
            setSentPhoto();
        }

        switch (answerIndex) {
            /*"At your place", 
            "McDonalds",
            "Guilty by Olivier",
            "A slaughterhouse"*/
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        jBrunoSix();
    }

    private void jBrunoSix() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno6);
        scanner.setMessage(JorgeBruno.sQuestion5);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno6[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nThat's me in the gym... Hope you like it! \n\n" + AsciiStrings.jBrunoGym);
            setSentPhoto();
        }

        switch (answerIndex) {
                 /* "Swing Party",
                    "Sailing around / Boat trip",
                    "Sushi & Drinks",
                    "Smoking crack behind a McDonald's"*/
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        jBrunoSeven();

    }

    private void jBrunoSeven() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno7);
        scanner.setMessage(JorgeBruno.sQuestion6);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno7[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nThat's me in the gym... Hope you like it! \n\n" + AsciiStrings.jBrunoGym);
            setSentPhoto();
        }

        switch (answerIndex) {
                /* "Wanna find out? ;)",
                 "BDSM",
                  "Ball busting",
                   "Pegging"*/
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter -= 5;
                break;
            case 4:
                happyMeter += 5;
                break;
        }
        if (happyMeter < 100) {
            jBrunoEight();
        } else {
            sendMessage.println("\nCongrats you're taking Jorge Bruno to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void jBrunoEight() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno8);
        scanner.setMessage(JorgeBruno.sQuestion7);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno8[answerIndex - 1]);
        /**
         * PIC SEND - v2
         * **/
        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nThat's me in the gym... Hope you like it! \n\n" + AsciiStrings.jBrunoGym);
            setSentPhoto();
        }

        switch (answerIndex) {
                                    /* "I tried to steal the Monalisa",
                                        "Got drunk and installed Tinder!",
                                        "Try to get it on with my friend’s parent",
                                        "Bought a new BMW"*/
            case 1:
                happyMeter -= 5;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter += 10;
                break;
        }
        if (happyMeter < 100) {
            jBrunoNine();
        } else {
            sendMessage.println("\nCongrats you're taking Jorge Bruno to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void jBrunoNine() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno9);
        scanner.setMessage(JorgeBruno.sQuestion8);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + JorgeBruno.jBruno9[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nThat's me in the gym... Hope you like it! \n\n" + AsciiStrings.jBrunoGym);
            setSentPhoto();
        }

        switch (answerIndex) {
            /*"Bigger than you...",
             "Why do you wanna know?",
              "I’m big enough",
               "Come find out!*/
            case 1:
                happyMeter -= 5;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        if (happyMeter < 100) {
            jBrunoTen();
        } else {
            sendMessage.println("\nCongrats you're taking Jorge Bruno to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void jBrunoTen() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(JorgeBruno.jBruno10);
        scanner.setMessage(JorgeBruno.sQuestion9);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + " sent a picture\n" + JorgeBruno.jBruno10[answerIndex - 1]);



        switch (answerIndex) {
            /*"Face",
              "Body",
              "Feet",
              "D*ck"*/
            case 1:
                happyMeter += 10;
                sendMessage.println(AsciiStrings.gigaFace);
                break;
            case 2:
                happyMeter += 15;
                sendMessage.println(AsciiStrings.body);
                break;
            case 3:
                happyMeter -= 5;
                sendMessage.println(AsciiStrings.feet);
                break;
            case 4:
                happyMeter += 5;
                sendMessage.println(AsciiStrings.duck);
                break;
        }
        if (happyMeter < 100) {
            sendMessage.println("\nYou're not very charming, bro. You lost your date.");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setEnded();
        } else if (happyMeter >= 100) {
            sendMessage.println("\nCongrats you're taking Jorge Bruno to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setEnded();
            setWon();
        }
    }
    private boolean hasWon = false;

    public boolean setWon() {
        return hasWon = true;
    }

    public boolean hasWon() {
        return hasWon;
    }

    private boolean hasEnded = false;

    public boolean hasEnded() {
        return hasEnded; }

    public boolean setEnded() {
        return hasEnded = true; }

    private boolean sentPhoto = false;

    public boolean setSentPhoto() {
        return sentPhoto = true;
    }
    public boolean hasSentPhoto() {
        return sentPhoto;
    }
}
