package org.academiadecodigo.bootcamp67;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp67.Men.JorgeBruno;
import org.academiadecodigo.bootcamp67.Woman.Constança;
import org.academiadecodigo.bootcamp67.Woman.Sheila;

import java.io.*;
import java.net.Socket;

public class UserConnection implements Runnable {
    private final Socket clientSocket;
    private final Server server;
    private String username;
    private PrintWriter sendMessage;
    private Sheila sheila;
    private Constança constança;
    private JorgeBruno jBruno;
    private String choice;
    private String chosenMatch;

    public UserConnection(Socket clientSocket, Server server) throws IOException {
        this.clientSocket = clientSocket;
        this.server = server;
    }

    public void usernameSet() throws IOException {
        this.sendMessage = new PrintWriter(this.clientSocket.getOutputStream(), true);
        BufferedReader reader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
        this.sendMessage.println("Choose your Username");
        this.username = reader.readLine();
        Thread.currentThread().setName(username);
    }

    public void introText() throws IOException {
        this.sendMessage = new PrintWriter(this.clientSocket.getOutputStream(), true);
        this.sendMessage.println(AsciiStrings.welcome + "\n" +
                "Waiting for more users");

    }

    /**
     * Main Menu
     **/
    public void menuPrompt() throws IOException {
        Prompt prompt = new Prompt
                (this.clientSocket.getInputStream(), new PrintStream(this.clientSocket.getOutputStream()));

        this.sendMessage = new PrintWriter(this.clientSocket.getOutputStream(), true);

        String[] options = new String[]{"Yes", "No"};
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage("Do you wanna swipe right on Sheila");

        int answerIndex = prompt.getUserInput(scanner);


        if (options[answerIndex - 1].equals("Yes")) {
            this.sendMessage.println(this.username + " swiped right on Sheila");
            setSheilaMatch();
            this.sheila = new Sheila(prompt, this.sendMessage, this.clientSocket, this.username);
            this.sheila.sheilaOne();
            this.choice = options[answerIndex - 1];
        }
        else if(options[answerIndex - 1].equals("No")) {
            this.sendMessage.println(this.username + " swiped left on Sheila");
            closeGame();
        }
    }

    public void run() {
        try {

            this.introText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        synchronized (server) {
            while (!server.isReady()) {
                try {

                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            this.usernameSet();
            readyToRun();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeGame() throws IOException {
        clientSocket.getOutputStream().close();
        clientSocket.getInputStream().close();
        server.endServer();
    }


    public void sheilaMatched() throws IOException {
        if (hasSheilaWon()) {
            server.sendServer("\n" + username + " has snatched a date with Sheila," +
                    "\nThey won with: " + sheila.getHappyMeter() + " points.");
            closeGame();
        }


    }

    public void jBrunoMatched() throws IOException {
        if (hasJbrunoWon()) {
            server.sendServer("\n" + username + " has snatched a date with Jorge Bruno," +
                    "\nThey won with: " + jBruno.getHappyMeter() + " points.");
            closeGame();
        }


    }

    public void constancaMatched() throws IOException {
        if (hasConstancaWon()) {
            server.sendServer("\n" + username + " has snatched a date with Constança," +
                    "\nThey won with: " + constança.getHappyMeter() + " points.");
            closeGame();
             }

    }

    private void readyToRun() throws IOException {

        while (true) {

            this.menuPrompt();

            if (hasMatchedJbruno()) {

                jBrunoMatched();


                break;
            }


            if (hasMatchedSheila()) {

              sheilaMatched();

                break;
            }

            if (hasMatchedConstanca()) {
                constancaMatched();
                break;

            }
            if (sheila.hasEnded() || jBruno.hasEnded() || jBruno.hasEnded()) {
                closeGame();
            }

        }


    }


    public void send(String message) throws IOException {
        this.sendMessage = new PrintWriter(this.clientSocket.getOutputStream(), true);
        this.sendMessage.println(message);
    }

    private boolean hasSheilaWon() {

        return sheila.hasWon();

    }

    private boolean hasConstancaWon() {

        return constança.hasWon();

    }

    private boolean hasJbrunoWon() {

        return jBruno.hasWon();

    }

    private boolean sheilaMatch = false;

    public boolean setSheilaMatch() {
        return sheilaMatch = true;
    }

    public boolean hasMatchedSheila() {
        return sheilaMatch;
    }

    private boolean constancaMatch = false;

    public boolean setConstancaMatch() {
        return constancaMatch = true;
    }

    public boolean hasMatchedConstanca() {
        return constancaMatch;
    }


    private boolean jBrunoMatch = false;

    public boolean setJbrunoMatch() {
        return jBrunoMatch = true;
    }

    public boolean hasMatchedJbruno() {
        return jBrunoMatch;
    }

}