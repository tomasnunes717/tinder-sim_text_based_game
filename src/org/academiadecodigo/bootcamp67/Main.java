package org.academiadecodigo.bootcamp67;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        Server gameServer = new Server(6000);
        gameServer.runServer();

    }
}
