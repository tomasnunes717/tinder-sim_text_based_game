package org.academiadecodigo.bootcamp67.Woman;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp67.AsciiStrings;
import org.academiadecodigo.bootcamp67.UserConnection;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Sheila {
    private Prompt prompt;
    private final PrintWriter sendMessage;
    private final Socket clientSocket;
    private final String username;
    private int happyMeter = 0;
    private UserConnection userConnection;


    public Sheila(Prompt prompt, PrintWriter sendMessage, Socket clientSocket, String username) {
        this.prompt = prompt;
        this.sendMessage = sendMessage;
        this.clientSocket = clientSocket;
        this.username = username;
        this.happyMeter = happyMeter;
    }

    public static String opener = "What's your opener?";
    public static String bio =
                    "--------------------------------------------------------------------\n" +
                    "|            Hi, my name is Sheila. I'm 42 years old               |\n" +
                    "|     My ex husband divorced me just because I like to have fun    |\n" +
                    "|                Do you wanna have fun with me?                    |\n"+
                    "--------------------------------------------------------------------\n";
    public static String sQuestion1 = "What do you do?";
    public static String sQuestion2 = "What do you listen to?";
    public static String sQuestion3 = "What are your hobbies?";
    public static String sQuestion4 = "Where would you take me on a date?";
    public static String sQuestion5 = "What would your ideal date be?";
    public static String sQuestion6 = "What are your kinks?";
    public static String sQuestion7 = "What is your drunkest memory?";
    public static String sQuestion8 = "How big are you?";
    public static String sQuestion9 = "Send me a pic!";

    public static String[] sheila1 = {"Hey Beautiful!",
            "Have you seen a rainbow? Gonna take a pic of my toenails",
            "Do you play Roblox?",
            "Let’s get wasted!"};

    public static String[] sheila2 = {"Manage/Work at my family’s company", "I study at Academia de Codigo",
            "Graphic Design is my passion", "Podiatrist"};

    public static String[] sheila3 = {"Fusion between k-pop, reggaeton and salsa", "Company Jingles", "Techno & Funk",
            "Indie Rock and Trap"};

    public static String[] sheila4 = {"Skateboard", "Clipping nails", "Airsoft", "Petting pussies"};

    public static String[] sheila5 = {"At your place", "McDonalds", "Guilty by Olivier", "A slaughterhouse"};

    public static String[] sheila6 = {"Swing Party", "Sailing around / Boat trip", "Sushi & Drinks",
            "Smoking crack behind a McDonald's"};

    public static String[] sheila7 = {"Wanna find out? ;)", "BDSM", "69 with our feet", "MILFS"};

    public static String[] sheila8 = {"I tried to steal the Monalisa", "Got drunk and installed Tinder!",
            "Try to get it on with my friend’s mom", "Bought a new BMW"};

    public static String[] sheila9 = {"Average", "Why do you wanna know?", "I’m 1,90m", "12"};

    public static String[] sheila10 = {"Face", "Body", "Feet", "D*ck"};


    public int getHappyMeter() {
        return happyMeter;
    }

    public void sheilaOne() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila1);
        scanner.setMessage(Sheila.bio);
       // scanner.setMessage(Sheila.opener);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila1[answerIndex - 1]);

        switch (answerIndex) {
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        sheilaTwo();
    }

    private void sheilaTwo() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila2);
        scanner.setMessage(Sheila.sQuestion1);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila2[answerIndex - 1]);

        switch (answerIndex) {
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 15;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        sheilaThree();
    }

    private void sheilaThree() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila3);
        scanner.setMessage(Sheila.sQuestion2);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila3[answerIndex - 1]);

        switch (answerIndex) {
            case 1:
                happyMeter += 5;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter += 10;
                break;
        }
        sheilaFour();
    }

    private void sheilaFour() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila4);
        scanner.setMessage(Sheila.sQuestion3);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila4[answerIndex - 1]);

        switch (answerIndex) {
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 10;
                break;
            case 4:
                happyMeter += 5;
                break;
        }
        sheilaFive();
    }

    private void sheilaFive() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila5);
        scanner.setMessage(Sheila.sQuestion4);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila5[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nHmmm, you seem fun. Here you deserve it :)\n\n" + AsciiStrings.sheilaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        sheilaSix();
    }

    private void sheilaSix() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila6);
        scanner.setMessage(Sheila.sQuestion5);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila6[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nHmmm, you seem fun. Here you deserve it :)\n\n" + AsciiStrings.sheilaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        sheilaSeven();

    }

    private void sheilaSeven() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila7);
        scanner.setMessage(Sheila.sQuestion6);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + " : " + Sheila.sheila7[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nHmmm, you seem fun. Here you deserve it :)\n\n" + AsciiStrings.sheilaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter -= 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        if (happyMeter < 100) {
            sheilaEight();
        } else {
            sendMessage.println("\nCongrats you're taking Sheila to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void sheilaEight() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila8);
        scanner.setMessage(Sheila.sQuestion7);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila8[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nHmmm, you seem fun. Here you deserve it :)\n\n" + AsciiStrings.sheilaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter -= 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        if (happyMeter < 100) {
            sheilaNine();
        } else {
            sendMessage.println("\nCongrats you're taking Sheila to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void sheilaNine() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila9);
        scanner.setMessage(Sheila.sQuestion8);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Sheila.sheila9[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nHmmm, you seem fun. Here you deserve it :)\n\n" + AsciiStrings.sheilaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            case 1:
                happyMeter += 5;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        if (happyMeter < 100) {
            sheilaTen();
        } else {
            sendMessage.println("\nCongrats you're taking Sheila to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void sheilaTen() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Sheila.sheila10);
        scanner.setMessage(Sheila.sQuestion9);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + " sent a picture of their " + Sheila.sheila10[answerIndex - 1]);


        switch (answerIndex) {
            case 1:
                happyMeter += 5;
                sendMessage.println(AsciiStrings.gigaFace);
                break;
            case 2:
                happyMeter += 10;
                sendMessage.println(AsciiStrings.body);
                break;
            case 3:
                happyMeter -= 5;
                sendMessage.println(AsciiStrings.feet);
                break;
            case 4:
                happyMeter += 15;
                sendMessage.println(AsciiStrings.duck);
                break;
        }
        if (happyMeter < 100) {
            sendMessage.println("\nYou're not very charming, bro. You lost your date.");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setEnded();
        } else if (happyMeter >= 100) {
            sendMessage.println("\nCongrats you're taking Sheila to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setEnded();
            setWon();
        }
    }


    private boolean hasWon = false;

    public boolean setWon() {
        return hasWon = true;
    }
    public boolean hasWon() {
        return hasWon;
    }

    private boolean hasEnded = false;

    public boolean hasEnded() {
        return hasEnded;
    }

    public boolean setEnded() {
        return hasEnded = true;
    }

    private boolean sentPhoto = false;

    public boolean setSentPhoto() {
        return sentPhoto = true;
    }
    public boolean hasSentPhoto() {
        return sentPhoto;
    }
}
