package org.academiadecodigo.bootcamp67.Woman;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp67.AsciiStrings;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Constança {
    private Prompt prompt;
    private final PrintWriter sendMessage;
    private final Socket clientSocket;
    private final String username;
    private int happyMeter = 0;

    public Constança(Prompt prompt, PrintWriter sendMessage, Socket clientSocket, String username) {
        this.prompt = prompt;
        this.sendMessage = sendMessage;
        this.clientSocket = clientSocket;
        this.username = username;
        this.happyMeter = happyMeter;
    }

    public static String opener = "What's your opener?";
    public static String bio =
            "--------------------------------------------------------------------\n" +
            "|   Heyyy, I'm Constança. I'm 25 years old, and I'm from Cascais   |\n" +
            "|     I'm here looking for someone to take me to their yacht       |\n" +
            "|          I have refined tastes, and you MUST be tall             |\n"+
            "--------------------------------------------------------------------\n";
    public static String sQuestion1 = "What do you do?";
    public static String sQuestion2 = "What do you listen to?";
    public static String sQuestion3 = "What are your hobbies?";
    public static String sQuestion4 = "Where would you take me on a date?";
    public static String sQuestion5 = "What would your ideal date be?";
    public static String sQuestion6 = "What are your kinks?";
    public static String sQuestion7 = "What is your drunkest memory?";
    public static String sQuestion8 = "How big are you?";
    public static String sQuestion9 = "Send me a pic!";

    public static String[] constanca1 = {"Hey Beautiful!",
                                        "Have you seen a rainbow? Gonna take a pic of my toenails",
                                        "Do you play Roblox?",
                                        "Let’s get wasted!"};

    public static String[] constanca2 = {"Manage/Work at my family’s company",
                                        "I study at Academia de Codigo",
                                        "Graphic Design is my passion",
                                        "Podiatrist"};

    public static String[] constanca3 = {"Fusion between k-pop, reggaeton and salsa",
                                        "Company Jingles",
                                        "Techno & Funk",
                                        "Indie Rock and Trap"};

    public static String[] constanca4 = {"Skateboard", "Clipping nails", "Airsoft", "Petting pussies"};

    public static String[] constanca5 = {"At your place", "McDonalds", "Guilty by Olivier", "A slaughterhouse"};

    public static String[] constanca6 = {"Swing Party",
                                        "Sailing around / Boat trip",
                                        "Sushi & Drinks",
                                        "Smoking crack behind a McDonald's"};

    public static String[] constanca7 = {"Wanna find out? ;)", "BDSM", "69 made of feet", "MILFS"};

    public static String[] constanca8 = {"I tried to steal the Monalisa",
                                        "Got drunk and installed Tinder!",
                                        "Try to get it on with my friend’s mom",
                                        "Bought a new BMW"};

    public static String[] constanca9 = {"Average", "Why do you wanna know?", "I’m 1,90m", "12"};

    public static String[] constanca10 = {"Face", "Body", "Feet", "D*ck"};


    public int getHappyMeter() {
        return happyMeter;
    }

    public void constancaOne() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca1);
        scanner.setMessage(Constança.bio);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca1[answerIndex - 1]);

        switch (answerIndex) {
                                        /*Hey Beautiful!",
                                        "Have you seen a rainbow? Gonna take a pic of my toenails",
                                        "Do you play Roblox?",
                                        "Let’s get wasted!"};*/
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 5;
                break;
            case 4:
                happyMeter += 10;
                break;
        }
        constancaTwo();
    }

    private void constancaTwo() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca2);
        scanner.setMessage(Constança.sQuestion1);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca2[answerIndex - 1]);

        switch (answerIndex) {
                /*  "Manage/Work at my family’s company",
                    "I study at Academia de Codigo",
                    "Graphic Design is my passion",
                    "Podiatrist"*/
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter -= 5;
                break;
            case 4:
                happyMeter += 10;
                break;
        }
        constancaThree();
    }

    private void constancaThree() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca3);
        scanner.setMessage(Constança.sQuestion2);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca3[answerIndex - 1]);

        switch (answerIndex) {
                  /*"Fusion between k-pop, reggaeton and salsa",
                    "Company Jingles",
                    "Techno & Funk",
                    "Indie Rock and Trap"*/
            case 1:
                happyMeter -= 5;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter += 10;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        constancaFour();
    }

    private void constancaFour() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca4);
        scanner.setMessage(Constança.sQuestion3);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca4[answerIndex - 1]);

        switch (answerIndex) {

                    /*"Skateboard",
                    "Clipping nails",
                    "Airsoft",
                    "Petting pussies"*/
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter -= 5;
                break;
            case 3:
                happyMeter += 10;
                break;
            case 4:
                happyMeter += 5;
                break;
        }
        constancaFive();
    }

    private void constancaFive() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca5);
        scanner.setMessage(Constança.sQuestion4);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca5[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nI like you... draw me like on of your french girls. \n\n" + AsciiStrings.constancaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            /*"At your place",
            "McDonalds",
            "Guilty by Olivier",
            "A slaughterhouse"*/
            case 1:
                happyMeter += 10;
                break;
            case 2:
                happyMeter += 5;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        constancaSix();
    }

    private void constancaSix() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca6);
        scanner.setMessage(Constança.sQuestion5);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca6[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nI like you... draw me like on of your french girls. \n\n" + AsciiStrings.constancaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
                 /* "Swing Party",
                    "Sailing around / Boat trip",
                    "Sushi & Drinks",
                    "Smoking crack behind a McDonald's"*/
            case 1:
                happyMeter += 5;
                break;
            case 2:
                happyMeter += 15;
                break;
            case 3:
                happyMeter += 10;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        constancaSeven();

    }

    private void constancaSeven() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca7);
        scanner.setMessage(Constança.sQuestion6);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca7[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nI like you... draw me like on of your french girls. \n\n" + AsciiStrings.constancaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
                /* "Wanna find out? ;)",
                   "BDSM",
                   "69 made of feet",
                   "MILFS"*/
            case 1:
                happyMeter += 15;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter -= 5;
                break;
            case 4:
                happyMeter += 5;
                break;
        }
        if (happyMeter < 100) {
            constancaEight();
        } else {
            sendMessage.println("\nCongrats you're taking Constança to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void constancaEight() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca8);
        scanner.setMessage(Constança.sQuestion7);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca8[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nI like you... draw me like on of your french girls. \n\n" + AsciiStrings.constancaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
                                    /* "I tried to steal the Monalisa",
                                        "Got drunk and installed Tinder!",
                                        "Try to get it on with my friend’s mom",
                                        "Bought a new BMW"*/
            case 1:
                happyMeter += 5;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter -= 5;
                break;
            case 4:
                happyMeter += 15;
                break;
        }
        if (happyMeter < 100) {
            constancaNine();
        } else {
            sendMessage.println("\nCongrats you're taking Constança to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void constancaNine() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca9);
        scanner.setMessage(Constança.sQuestion8);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + ": " + Constança.constanca9[answerIndex - 1]);

        /**
         * PIC SEND - v1
         * **/
        if (happyMeter >= 70 && !hasSentPhoto()) {
            sendMessage.println("\n\n\nI like you... draw me like on of your french girls. \n\n" + AsciiStrings.constancaBod);
            setSentPhoto();
        }

        switch (answerIndex) {
            /*"Average",
              "Why do you wanna know?",
              "I’m 1,90m",
              "12"*/
            case 1:
                happyMeter += 5;
                break;
            case 2:
                happyMeter += 10;
                break;
            case 3:
                happyMeter += 15;
                break;
            case 4:
                happyMeter -= 5;
                break;
        }
        if (happyMeter < 100) {
            constancaTen();
        } else {
            sendMessage.println("\nCongrats you're taking Constança to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setWon();
        }
    }

    private void constancaTen() throws IOException {
        prompt = new Prompt(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(Constança.constanca10);
        scanner.setMessage(Constança.sQuestion9);

        // show the menu to the user and get the selected answer
        int answerIndex = prompt.getUserInput(scanner);
        sendMessage.println(username + " sent a picture\n" + Constança.constanca10[answerIndex - 1]);



        switch (answerIndex) {
            /*"Face",
              "Body",
              "Feet",
              "D*ck"*/
            case 1:
                happyMeter += 15;
                sendMessage.println(AsciiStrings.gigaFace);
                break;
            case 2:
                happyMeter += 10;
                sendMessage.println(AsciiStrings.body);
                break;
            case 3:
                happyMeter -= 5;
                sendMessage.println(AsciiStrings.feet);
                break;
            case 4:
                happyMeter += 5;
                sendMessage.println(AsciiStrings.duck);
                break;
        }
        if (happyMeter < 100) {
            sendMessage.println("\nYou're not very charming, bro. You lost your date.");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setEnded();
        } else if (happyMeter >= 100) {
            sendMessage.println("\nCongrats you're taking Constança to a date");
            this.sendMessage.println("You got " + happyMeter + " love points");
            setEnded();
            setWon();
        }
    }
    private boolean hasWon = false;

    public boolean setWon() {
        return hasWon = true;
    }

    public boolean hasWon() {
        return hasWon;
    }

    private boolean hasEnded = false;

    public boolean hasEnded() {
        return hasEnded; }

    public boolean setEnded() {
        return hasEnded = true; }

    private boolean sentPhoto = false;

    public boolean setSentPhoto() {
        return sentPhoto = true;
    }
    public boolean hasSentPhoto() {
        return sentPhoto;
    }

}
