package org.academiadecodigo.bootcamp67;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private final LinkedList<UserConnection> userConnections = new LinkedList<>();
    private ServerSocket serverSocket;
    ExecutorService fixedPool;
    private boolean ready;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void sendServer(String str) throws IOException {
        synchronized (this) {
            for (UserConnection cc : userConnections) {
                cc.send(str);
                endServer();
            }
        }
    }

    public void userDisconnect(UserConnection connection) {
        userConnections.remove(connection);
    }

    public void endServer() throws IOException {

        serverSocket.close();
        System.exit(0);
    }

    public void runServer() {

        System.out.println("Server is waiting for Users to connect on port 6000");

        int numUsers = 2;

        fixedPool = Executors.newFixedThreadPool(numUsers);
        synchronized (this) {
            while (userConnections.size() < numUsers) {
                try {
                    if (serverSocket.isClosed()) {
                        return;
                    }

                    UserConnection userConnection = new UserConnection(serverSocket.accept(), this);
                    userConnections.add(userConnection);
                    fixedPool.submit(userConnection);
                    System.out.println("Connected");

                } catch (IOException e) {
                    e.getSuppressed();
                }
            }
            System.out.println("Client connections: " + userConnections.size());
            setReady(true);
            notifyAll();
        }
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }
}